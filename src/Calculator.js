

export class Calculator {
    /**
     * Méthode pourrie qui additionne
     * @param {number} a le premier nombre
     * @param {number} b le deuxième nombre
     * @returns {number} le résultat de l'addition
     */
    add(a,b) {
        // if(Number.isNaN(a) || Number.isNaN(b)) {
        if(typeof(a) !== 'number' || typeof(b) !== 'number') {
            throw new Error('Incorrect argument. Number expected.');
        }
        return a+b;
    }
}