import { SmartBin } from "./SmartBin";


export class ViewBin {
    /**
     * 
     * @param {string} selector Sélécteur css de l'élément où mettre l'affichage
     * @param {number} volume le volume de la poubelle
     * @param {string} color  la couleur de la poubelle
     */
    constructor(selector, volume, color) {
        this.selector = selector;
        this.smartBin = new SmartBin(volume, color);
    }

    /**
     * Méthode qui génère le html de la poubelle en propriété
     * @returns {Element} L'élément html généré
     */
    draw() {
        let target = document.querySelector(this.selector);
        target.innerHTML = '';

        let element = document.createElement('div');
        element.classList.add('smart-bin');
        element.textContent = 'Actual Bin Volume : ' + this.smartBin.totalVolume()
        element.style.backgroundColor = this.color;
        this.drawWeight(element);
        target.appendChild(element);

        return element;
    }

    /**
     * Méthode qui génère le html représentant le poid des déchets
     * @param {Element} element L'élément html où append le poid des déchets
     */
    drawWeight(element) {
        let total = this.smartBin.total();
        let pGrey = document.createElement('p');
        pGrey.textContent = 'Grey : ' + total.grey + ' gr';
        element.appendChild(pGrey);

        let pGlass = document.createElement('p');
        pGlass.textContent = 'Glass : ' + total.glass + ' gr';
        element.appendChild(pGlass);

        let pGreen = document.createElement('p');
        pGreen.textContent = 'Green : ' + total.green + ' gr';
        element.appendChild(pGreen);

        let pYellow = document.createElement('p');
        pYellow.textContent = 'Yellow : ' + total.yellow + ' gr';
        element.appendChild(pYellow);
    }


}