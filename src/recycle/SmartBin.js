import { Waste } from "./Waste";

export class SmartBin {
    /**
     * @param {number} volume le volume max de la poubelle
     * @param {string} color la couleur esthétique de la poubelle
     * @param {Waste[]} content le contenu de la poubelle
     */
    constructor(volume, color, content = []) {
            this.volume = volume;
            this.color = color;
            this.content = content;
        }
        /**
         * Méthode pour vider la poubelle
         */
    empty() {
            this.content = [];
        }
        /**
         * Méthode pour mettre un déchet à la poubelle
         * @param {Waste} waste le déchet à mettre à la poubelle
         */
    add(waste) {
        if (parseInt(waste.volume) + parseInt(this.totalVolume()) <= parseInt(this.volume)) {
            this.content.push(waste);
        }
    }

    /**
     * Méthode qui calcul le volume actuel de tous les déchets contenus
     * dans la poubelle
     * @returns {number} le volume total
     */
    totalVolume() {
        let actualVolume = 0;
        for (const iterator of this.content) {
            actualVolume += parseInt(iterator.volume);
        }
        return actualVolume;
        // return this.content.reduce((acc, cur) => acc+= cur.volume, 0);
    }

    /**
     * Méthode qui nous indique le poids des différents types de déchets
     * @returns {object} le bilan de ce qu'on a dans la poubelle
     */
    total() {
        let grey = 0;
        let yellow = 0;
        let green = 0;
        let glass = 0;
        for (const iterator of this.content) {
            if (!iterator.recyclable && iterator.type !== 'organic') {
                grey += parseInt(iterator.weight);

            }
            if (iterator.recyclable && (iterator.type === 'paper' || iterator.type === 'plastic')) {
                yellow += parseInt(iterator.weight);

            }
            if (iterator.type === 'organic') {
                green += parseInt(iterator.weight);

            }
            if (iterator.type === 'glass' && iterator.recyclable) {
                glass += parseInt(iterator.weight);

            }
        }
        return { grey, yellow, green, glass };
    }
}