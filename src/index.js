import { ViewBin } from "./recycle/ViewBin";
import { Waste } from "./recycle/Waste";
import { SmartBin } from "./recycle/SmartBin";


let view = new ViewBin('#target');
view.smartBin = new SmartBin(10, 'green', []);

view.draw();



let addWasteBtn = document.querySelector('#add-waste');


addWasteBtn.addEventListener("click", function() {
    let inputWeight = document.querySelector('#waste-weight');
    let inputVolume = document.querySelector('#waste-volume');
    let inputType = document.querySelector('#waste-type').value;
    let inputRecyclable = document.querySelector('#recyclable').checked;

    let newWaste = new Waste(
        parseInt(inputWeight.value),
        parseInt(inputVolume.value),
        inputType,
        inputRecyclable);

    view.smartBin.add(newWaste);
    view.draw();

});