/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _recycle_ViewBin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./recycle/ViewBin */ "./src/recycle/ViewBin.js");
/* harmony import */ var _recycle_Waste__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recycle/Waste */ "./src/recycle/Waste.js");
/* harmony import */ var _recycle_SmartBin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recycle/SmartBin */ "./src/recycle/SmartBin.js");





let view = new _recycle_ViewBin__WEBPACK_IMPORTED_MODULE_0__["ViewBin"]('#target');
view.smartBin = new _recycle_SmartBin__WEBPACK_IMPORTED_MODULE_2__["SmartBin"](10, 'green', []);

view.draw();



let addWasteBtn = document.querySelector('#add-waste');


addWasteBtn.addEventListener("click", function() {
    let inputWeight = document.querySelector('#waste-weight');
    let inputVolume = document.querySelector('#waste-volume');
    let inputType = document.querySelector('#waste-type').value;
    let inputRecyclable = document.querySelector('#recyclable').checked;

    let newWaste = new _recycle_Waste__WEBPACK_IMPORTED_MODULE_1__["Waste"](
        parseInt(inputWeight.value),
        parseInt(inputVolume.value),
        inputType,
        inputRecyclable);

    view.smartBin.add(newWaste);
    view.draw();

});

/***/ }),

/***/ "./src/recycle/SmartBin.js":
/*!*********************************!*\
  !*** ./src/recycle/SmartBin.js ***!
  \*********************************/
/*! exports provided: SmartBin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartBin", function() { return SmartBin; });
/* harmony import */ var _Waste__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Waste */ "./src/recycle/Waste.js");


class SmartBin {
    /**
     * @param {number} volume le volume max de la poubelle
     * @param {string} color la couleur esthétique de la poubelle
     * @param {Waste[]} content le contenu de la poubelle
     */
    constructor(volume, color, content = []) {
            this.volume = volume;
            this.color = color;
            this.content = content;
        }
        /**
         * Méthode pour vider la poubelle
         */
    empty() {
            this.content = [];
        }
        /**
         * Méthode pour mettre un déchet à la poubelle
         * @param {Waste} waste le déchet à mettre à la poubelle
         */
    add(waste) {
        if (parseInt(waste.volume) + parseInt(this.totalVolume()) <= parseInt(this.volume)) {
            this.content.push(waste);
        }
    }

    /**
     * Méthode qui calcul le volume actuel de tous les déchets contenus
     * dans la poubelle
     * @returns {number} le volume total
     */
    totalVolume() {
        let actualVolume = 0;
        for (const iterator of this.content) {
            actualVolume += parseInt(iterator.volume);
        }
        return actualVolume;
        // return this.content.reduce((acc, cur) => acc+= cur.volume, 0);
    }

    /**
     * Méthode qui nous indique le poids des différents types de déchets
     * @returns {object} le bilan de ce qu'on a dans la poubelle
     */
    total() {
        let grey = 0;
        let yellow = 0;
        let green = 0;
        let glass = 0;
        for (const iterator of this.content) {
            if (!iterator.recyclable && iterator.type !== 'organic') {
                grey += parseInt(iterator.weight);

            }
            if (iterator.recyclable && (iterator.type === 'paper' || iterator.type === 'plastic')) {
                yellow += parseInt(iterator.weight);

            }
            if (iterator.type === 'organic') {
                green += parseInt(iterator.weight);

            }
            if (iterator.type === 'glass' && iterator.recyclable) {
                glass += parseInt(iterator.weight);

            }
        }
        return { grey, yellow, green, glass };
    }
}

/***/ }),

/***/ "./src/recycle/ViewBin.js":
/*!********************************!*\
  !*** ./src/recycle/ViewBin.js ***!
  \********************************/
/*! exports provided: ViewBin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewBin", function() { return ViewBin; });
/* harmony import */ var _SmartBin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SmartBin */ "./src/recycle/SmartBin.js");



class ViewBin {
    /**
     * 
     * @param {string} selector Sélécteur css de l'élément où mettre l'affichage
     * @param {number} volume le volume de la poubelle
     * @param {string} color  la couleur de la poubelle
     */
    constructor(selector, volume, color) {
        this.selector = selector;
        this.smartBin = new _SmartBin__WEBPACK_IMPORTED_MODULE_0__["SmartBin"](volume, color);
    }

    /**
     * Méthode qui génère le html de la poubelle en propriété
     * @returns {Element} L'élément html généré
     */
    draw() {
        let target = document.querySelector(this.selector);
        target.innerHTML = '';

        let element = document.createElement('div');
        element.classList.add('smart-bin');
        element.textContent = 'Actual Bin Volume : ' + this.smartBin.totalVolume()
        element.style.backgroundColor = this.color;
        this.drawWeight(element);
        target.appendChild(element);

        return element;
    }

    /**
     * Méthode qui génère le html représentant le poid des déchets
     * @param {Element} element L'élément html où append le poid des déchets
     */
    drawWeight(element) {
        let total = this.smartBin.total();
        let pGrey = document.createElement('p');
        pGrey.textContent = 'Grey : ' + total.grey + ' gr';
        element.appendChild(pGrey);

        let pGlass = document.createElement('p');
        pGlass.textContent = 'Glass : ' + total.glass + ' gr';
        element.appendChild(pGlass);

        let pGreen = document.createElement('p');
        pGreen.textContent = 'Green : ' + total.green + ' gr';
        element.appendChild(pGreen);

        let pYellow = document.createElement('p');
        pYellow.textContent = 'Yellow : ' + total.yellow + ' gr';
        element.appendChild(pYellow);
    }


}

/***/ }),

/***/ "./src/recycle/Waste.js":
/*!******************************!*\
  !*** ./src/recycle/Waste.js ***!
  \******************************/
/*! exports provided: Waste */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Waste", function() { return Waste; });
class Waste {
    /**
     * @param {number} weight le poid du déchet
     * @param {number} volume le volume que prend le déchet
     * @param {string} type la matière du déchet (plastic, glass, organic, other, paper)
     * @param {boolean} recyclable si le déchet est recyclable ou pas
     */
    constructor(weight, volume, type, recyclable) {
        this.weight = weight;
        this.type = type;
        this.volume = volume;
        this.recyclable = recyclable;
    }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9yZWN5Y2xlL1NtYXJ0QmluLmpzIiwid2VicGFjazovLy8uL3NyYy9yZWN5Y2xlL1ZpZXdCaW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JlY3ljbGUvV2FzdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUE0QztBQUNKO0FBQ007OztBQUc5QyxlQUFlLHdEQUFPO0FBQ3RCLG9CQUFvQiwwREFBUTs7QUFFNUI7Ozs7QUFJQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx1QkFBdUIsb0RBQUs7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxDQUFDLEU7Ozs7Ozs7Ozs7OztBQzlCRDtBQUFBO0FBQUE7QUFBZ0M7O0FBRXpCO0FBQ1A7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLE1BQU07QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUN4RUE7QUFBQTtBQUFBO0FBQXNDOzs7QUFHL0I7QUFDUDtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLGtEQUFRO0FBQ3BDOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsUUFBUTtBQUN6QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZUFBZSxRQUFRO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxDOzs7Ozs7Ozs7Ozs7QUN6REE7QUFBQTtBQUFPO0FBQ1A7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IFZpZXdCaW4gfSBmcm9tIFwiLi9yZWN5Y2xlL1ZpZXdCaW5cIjtcbmltcG9ydCB7IFdhc3RlIH0gZnJvbSBcIi4vcmVjeWNsZS9XYXN0ZVwiO1xuaW1wb3J0IHsgU21hcnRCaW4gfSBmcm9tIFwiLi9yZWN5Y2xlL1NtYXJ0QmluXCI7XG5cblxubGV0IHZpZXcgPSBuZXcgVmlld0JpbignI3RhcmdldCcpO1xudmlldy5zbWFydEJpbiA9IG5ldyBTbWFydEJpbigxMCwgJ2dyZWVuJywgW10pO1xuXG52aWV3LmRyYXcoKTtcblxuXG5cbmxldCBhZGRXYXN0ZUJ0biA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNhZGQtd2FzdGUnKTtcblxuXG5hZGRXYXN0ZUJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gICAgbGV0IGlucHV0V2VpZ2h0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3dhc3RlLXdlaWdodCcpO1xuICAgIGxldCBpbnB1dFZvbHVtZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyN3YXN0ZS12b2x1bWUnKTtcbiAgICBsZXQgaW5wdXRUeXBlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3dhc3RlLXR5cGUnKS52YWx1ZTtcbiAgICBsZXQgaW5wdXRSZWN5Y2xhYmxlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3JlY3ljbGFibGUnKS5jaGVja2VkO1xuXG4gICAgbGV0IG5ld1dhc3RlID0gbmV3IFdhc3RlKFxuICAgICAgICBwYXJzZUludChpbnB1dFdlaWdodC52YWx1ZSksXG4gICAgICAgIHBhcnNlSW50KGlucHV0Vm9sdW1lLnZhbHVlKSxcbiAgICAgICAgaW5wdXRUeXBlLFxuICAgICAgICBpbnB1dFJlY3ljbGFibGUpO1xuXG4gICAgdmlldy5zbWFydEJpbi5hZGQobmV3V2FzdGUpO1xuICAgIHZpZXcuZHJhdygpO1xuXG59KTsiLCJpbXBvcnQgeyBXYXN0ZSB9IGZyb20gXCIuL1dhc3RlXCI7XG5cbmV4cG9ydCBjbGFzcyBTbWFydEJpbiB7XG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHZvbHVtZSBsZSB2b2x1bWUgbWF4IGRlIGxhIHBvdWJlbGxlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGNvbG9yIGxhIGNvdWxldXIgZXN0aMOpdGlxdWUgZGUgbGEgcG91YmVsbGVcbiAgICAgKiBAcGFyYW0ge1dhc3RlW119IGNvbnRlbnQgbGUgY29udGVudSBkZSBsYSBwb3ViZWxsZVxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKHZvbHVtZSwgY29sb3IsIGNvbnRlbnQgPSBbXSkge1xuICAgICAgICAgICAgdGhpcy52b2x1bWUgPSB2b2x1bWU7XG4gICAgICAgICAgICB0aGlzLmNvbG9yID0gY29sb3I7XG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQgPSBjb250ZW50O1xuICAgICAgICB9XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNw6l0aG9kZSBwb3VyIHZpZGVyIGxhIHBvdWJlbGxlXG4gICAgICAgICAqL1xuICAgIGVtcHR5KCkge1xuICAgICAgICAgICAgdGhpcy5jb250ZW50ID0gW107XG4gICAgICAgIH1cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE3DqXRob2RlIHBvdXIgbWV0dHJlIHVuIGTDqWNoZXQgw6AgbGEgcG91YmVsbGVcbiAgICAgICAgICogQHBhcmFtIHtXYXN0ZX0gd2FzdGUgbGUgZMOpY2hldCDDoCBtZXR0cmUgw6AgbGEgcG91YmVsbGVcbiAgICAgICAgICovXG4gICAgYWRkKHdhc3RlKSB7XG4gICAgICAgIGlmIChwYXJzZUludCh3YXN0ZS52b2x1bWUpICsgcGFyc2VJbnQodGhpcy50b3RhbFZvbHVtZSgpKSA8PSBwYXJzZUludCh0aGlzLnZvbHVtZSkpIHtcbiAgICAgICAgICAgIHRoaXMuY29udGVudC5wdXNoKHdhc3RlKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSBjYWxjdWwgbGUgdm9sdW1lIGFjdHVlbCBkZSB0b3VzIGxlcyBkw6ljaGV0cyBjb250ZW51c1xuICAgICAqIGRhbnMgbGEgcG91YmVsbGVcbiAgICAgKiBAcmV0dXJucyB7bnVtYmVyfSBsZSB2b2x1bWUgdG90YWxcbiAgICAgKi9cbiAgICB0b3RhbFZvbHVtZSgpIHtcbiAgICAgICAgbGV0IGFjdHVhbFZvbHVtZSA9IDA7XG4gICAgICAgIGZvciAoY29uc3QgaXRlcmF0b3Igb2YgdGhpcy5jb250ZW50KSB7XG4gICAgICAgICAgICBhY3R1YWxWb2x1bWUgKz0gcGFyc2VJbnQoaXRlcmF0b3Iudm9sdW1lKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYWN0dWFsVm9sdW1lO1xuICAgICAgICAvLyByZXR1cm4gdGhpcy5jb250ZW50LnJlZHVjZSgoYWNjLCBjdXIpID0+IGFjYys9IGN1ci52b2x1bWUsIDApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSBub3VzIGluZGlxdWUgbGUgcG9pZHMgZGVzIGRpZmbDqXJlbnRzIHR5cGVzIGRlIGTDqWNoZXRzXG4gICAgICogQHJldHVybnMge29iamVjdH0gbGUgYmlsYW4gZGUgY2UgcXUnb24gYSBkYW5zIGxhIHBvdWJlbGxlXG4gICAgICovXG4gICAgdG90YWwoKSB7XG4gICAgICAgIGxldCBncmV5ID0gMDtcbiAgICAgICAgbGV0IHllbGxvdyA9IDA7XG4gICAgICAgIGxldCBncmVlbiA9IDA7XG4gICAgICAgIGxldCBnbGFzcyA9IDA7XG4gICAgICAgIGZvciAoY29uc3QgaXRlcmF0b3Igb2YgdGhpcy5jb250ZW50KSB7XG4gICAgICAgICAgICBpZiAoIWl0ZXJhdG9yLnJlY3ljbGFibGUgJiYgaXRlcmF0b3IudHlwZSAhPT0gJ29yZ2FuaWMnKSB7XG4gICAgICAgICAgICAgICAgZ3JleSArPSBwYXJzZUludChpdGVyYXRvci53ZWlnaHQpO1xuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoaXRlcmF0b3IucmVjeWNsYWJsZSAmJiAoaXRlcmF0b3IudHlwZSA9PT0gJ3BhcGVyJyB8fCBpdGVyYXRvci50eXBlID09PSAncGxhc3RpYycpKSB7XG4gICAgICAgICAgICAgICAgeWVsbG93ICs9IHBhcnNlSW50KGl0ZXJhdG9yLndlaWdodCk7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChpdGVyYXRvci50eXBlID09PSAnb3JnYW5pYycpIHtcbiAgICAgICAgICAgICAgICBncmVlbiArPSBwYXJzZUludChpdGVyYXRvci53ZWlnaHQpO1xuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoaXRlcmF0b3IudHlwZSA9PT0gJ2dsYXNzJyAmJiBpdGVyYXRvci5yZWN5Y2xhYmxlKSB7XG4gICAgICAgICAgICAgICAgZ2xhc3MgKz0gcGFyc2VJbnQoaXRlcmF0b3Iud2VpZ2h0KTtcblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB7IGdyZXksIHllbGxvdywgZ3JlZW4sIGdsYXNzIH07XG4gICAgfVxufSIsImltcG9ydCB7IFNtYXJ0QmluIH0gZnJvbSBcIi4vU21hcnRCaW5cIjtcblxuXG5leHBvcnQgY2xhc3MgVmlld0JpbiB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNlbGVjdG9yIFPDqWzDqWN0ZXVyIGNzcyBkZSBsJ8OpbMOpbWVudCBvw7kgbWV0dHJlIGwnYWZmaWNoYWdlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHZvbHVtZSBsZSB2b2x1bWUgZGUgbGEgcG91YmVsbGVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gY29sb3IgIGxhIGNvdWxldXIgZGUgbGEgcG91YmVsbGVcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihzZWxlY3Rvciwgdm9sdW1lLCBjb2xvcikge1xuICAgICAgICB0aGlzLnNlbGVjdG9yID0gc2VsZWN0b3I7XG4gICAgICAgIHRoaXMuc21hcnRCaW4gPSBuZXcgU21hcnRCaW4odm9sdW1lLCBjb2xvcik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIGfDqW7DqHJlIGxlIGh0bWwgZGUgbGEgcG91YmVsbGUgZW4gcHJvcHJpw6l0w6lcbiAgICAgKiBAcmV0dXJucyB7RWxlbWVudH0gTCfDqWzDqW1lbnQgaHRtbCBnw6luw6lyw6lcbiAgICAgKi9cbiAgICBkcmF3KCkge1xuICAgICAgICBsZXQgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLnNlbGVjdG9yKTtcbiAgICAgICAgdGFyZ2V0LmlubmVySFRNTCA9ICcnO1xuXG4gICAgICAgIGxldCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnc21hcnQtYmluJyk7XG4gICAgICAgIGVsZW1lbnQudGV4dENvbnRlbnQgPSAnQWN0dWFsIEJpbiBWb2x1bWUgOiAnICsgdGhpcy5zbWFydEJpbi50b3RhbFZvbHVtZSgpXG4gICAgICAgIGVsZW1lbnQuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gdGhpcy5jb2xvcjtcbiAgICAgICAgdGhpcy5kcmF3V2VpZ2h0KGVsZW1lbnQpO1xuICAgICAgICB0YXJnZXQuYXBwZW5kQ2hpbGQoZWxlbWVudCk7XG5cbiAgICAgICAgcmV0dXJuIGVsZW1lbnQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIGfDqW7DqHJlIGxlIGh0bWwgcmVwcsOpc2VudGFudCBsZSBwb2lkIGRlcyBkw6ljaGV0c1xuICAgICAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbWVudCBMJ8OpbMOpbWVudCBodG1sIG/DuSBhcHBlbmQgbGUgcG9pZCBkZXMgZMOpY2hldHNcbiAgICAgKi9cbiAgICBkcmF3V2VpZ2h0KGVsZW1lbnQpIHtcbiAgICAgICAgbGV0IHRvdGFsID0gdGhpcy5zbWFydEJpbi50b3RhbCgpO1xuICAgICAgICBsZXQgcEdyZXkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBHcmV5LnRleHRDb250ZW50ID0gJ0dyZXkgOiAnICsgdG90YWwuZ3JleSArICcgZ3InO1xuICAgICAgICBlbGVtZW50LmFwcGVuZENoaWxkKHBHcmV5KTtcblxuICAgICAgICBsZXQgcEdsYXNzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICBwR2xhc3MudGV4dENvbnRlbnQgPSAnR2xhc3MgOiAnICsgdG90YWwuZ2xhc3MgKyAnIGdyJztcbiAgICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZChwR2xhc3MpO1xuXG4gICAgICAgIGxldCBwR3JlZW4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBHcmVlbi50ZXh0Q29udGVudCA9ICdHcmVlbiA6ICcgKyB0b3RhbC5ncmVlbiArICcgZ3InO1xuICAgICAgICBlbGVtZW50LmFwcGVuZENoaWxkKHBHcmVlbik7XG5cbiAgICAgICAgbGV0IHBZZWxsb3cgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBZZWxsb3cudGV4dENvbnRlbnQgPSAnWWVsbG93IDogJyArIHRvdGFsLnllbGxvdyArICcgZ3InO1xuICAgICAgICBlbGVtZW50LmFwcGVuZENoaWxkKHBZZWxsb3cpO1xuICAgIH1cblxuXG59IiwiZXhwb3J0IGNsYXNzIFdhc3RlIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gd2VpZ2h0IGxlIHBvaWQgZHUgZMOpY2hldFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB2b2x1bWUgbGUgdm9sdW1lIHF1ZSBwcmVuZCBsZSBkw6ljaGV0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHR5cGUgbGEgbWF0acOocmUgZHUgZMOpY2hldCAocGxhc3RpYywgZ2xhc3MsIG9yZ2FuaWMsIG90aGVyLCBwYXBlcilcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IHJlY3ljbGFibGUgc2kgbGUgZMOpY2hldCBlc3QgcmVjeWNsYWJsZSBvdSBwYXNcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3Rvcih3ZWlnaHQsIHZvbHVtZSwgdHlwZSwgcmVjeWNsYWJsZSkge1xuICAgICAgICB0aGlzLndlaWdodCA9IHdlaWdodDtcbiAgICAgICAgdGhpcy50eXBlID0gdHlwZTtcbiAgICAgICAgdGhpcy52b2x1bWUgPSB2b2x1bWU7XG4gICAgICAgIHRoaXMucmVjeWNsYWJsZSA9IHJlY3ljbGFibGU7XG4gICAgfVxufSJdLCJzb3VyY2VSb290IjoiIn0=