/// <reference types="Cypress" />

import { Counter } from "../src/Counter";

describe('Counter class test', () => {
    let counter;
    beforeEach(() => {
        counter = new Counter();
    });

    it('should increment count', () => {
        counter.increment();
        expect(counter.count).to.equal(1);
    });

    it('should decrement count', () => {
        counter.count = 5;
        counter.decrement();
        expect(counter.count).to.equal(4);
    });

    it('should not decrement count beneath zero', () => {
        counter.decrement()
        expect(counter.count).to.equal(0);
    });

    it('should reset count', () => {
        counter.count = 5;
        counter.reset();
        expect(counter.count).to.equal(0);
    });
});