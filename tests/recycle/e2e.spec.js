/// <reference types="Cypress" />


describe('SmartBin Interface test', () => {

    it('should display the empty bin', () => {

        cy.visit('http://localhost/exos/JS/js-tests/js-test/');

        cy.get('p').should('have.length', 4);
        cy.get('p').first().should('contain.text', 'Grey : 0 gr');

    });

    it('should add a waste in the smartbin', () => {
        cy.get('#waste-weight').type('3');
        cy.get('#waste-volume').type('2');
        cy.get('#waste-type').select('organic');
        cy.get('#add-waste').click();
        cy.get('p').should('contain', 'Green : 3');
    });

    it('should not add a waste if it is too much volume', () => {
        cy.get('#waste-weight').clear().type('3');
        cy.get('#waste-volume').clear().type('10');
        cy.get('#waste-type').select('plastic');
        cy.get('#recyclable').check();
        cy.get('#add-waste').click();

        cy.get('div').should('contain', 'Actual Bin Volume : 2');


    });

})