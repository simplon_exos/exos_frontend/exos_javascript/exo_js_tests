/// <reference types="Cypress" />

import { Calculator } from "../src/Calculator";


describe('Calculator class test', () => {
    let calc;
    beforeEach(() => {
        calc = new Calculator();
    });

    it('should add two numbers', () => {
        let result = calc.add(2,2);
        expect(result).to.equal(4);
    });

    it('should throw error if NaN given', () => {
        expect(() => calc.add('bloup', 2))
        .to.throw('Incorrect argument. Number expected.');
    });

    /*it('should concatenate strings', () => {
        let result = calc.add('bloup','blip');
        
        expect(result).to.equal('bloupblip');
    });*/
});